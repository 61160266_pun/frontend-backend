const express = require('express')
const router = express.Router()

// eslint-disable-next-line no-unused-vars
router.get('/:message', (req, res, next) => {
    const { params } = req
    res.json({ message: 'Hello Kob obob!!', params })
})

module.exports = router
